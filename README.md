
# HTTP OK

This docker image hosts an http server that responds 200 OK to every request. x86 and ARM 64 images
are provided.

There is nothing to configure. It always listens on port 80 and responds with a 200 OK to every
request regardless of the request method, path, query parameters, or body. I built this to provide
a placeholder in AWS ECS that will pass load balancer health checks before the first CodeDeploy of
the application occurs.

The implementation is simply nginx with a response hard coded into the config.


# Links

- Source: https://gitlab.com/francisferrell/http-ok
- Docker Hub: https://hub.docker.com/r/francisferrell/http-ok

